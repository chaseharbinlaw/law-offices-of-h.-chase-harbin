H. Chase Harbin is a criminal and DUI defense lawyer advocating for clients in Greenville and Pickens, SC. The Law Offices of H. Chase Harbin is an authority on South Carolina criminal law and DUI law -- Call Harbin�s Pickens or Greenville office today for them to handle your case.

Address: 419 Vardry Street, Greenville, SC 29601, USA

Phone: 864-349-5356
